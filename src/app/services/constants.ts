import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})

export default class Constants {
  public PHONE_FORMAT: RegExp = /(\d{3})(\d{3})(\d{2})(\d2{2})*/;
}