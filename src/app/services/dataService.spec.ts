import { TestBed } from '@angular/core/testing';
import { DataService } from './dataService';
import LoaderService from './loaderService';
import { HttpClientTestingModule, HttpTestingController, TestRequest } from '@angular/common/http/testing';
import { environment } from 'src/environments/environments';
import { Contact } from '../models/contact.model';
import { Subscription } from 'rxjs';

describe('Loader service', () => {
  let loaderService: LoaderService;
  let dataService: DataService;
  let httpRequest: HttpTestingController;
  let dataSubscription: Subscription;
  const mockData: Contact[] = [
    {
      '_id': '1',
      'favourite': false,
      'firstName': 'Ivan',
      'secondNmae': 'Ivanov',
      'thirdName': 'Ivanovich',
      'phone': '79999999999',
    },
    {
      '_id': '2',
      'favourite': true,
      'firstName': 'Petr',
      'secondNmae': 'Petrov',
      'thirdName': 'Petrovich',
      'phone': '71111111111',
    }
  ]

  let gettedData: Contact[] = [];

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
      ],
    })

    httpRequest = TestBed.inject<HttpTestingController>(HttpTestingController);
    dataService = TestBed.inject<DataService>(DataService);
    loaderService = TestBed.inject<LoaderService>(LoaderService);
    dataSubscription = dataService.getContacts.subscribe(value => {
      gettedData = value;
    })
  });

  afterEach(() => {
    httpRequest  = null;
    dataService = null;
    loaderService = null;
    gettedData = null;
    dataSubscription.unsubscribe();
  })

  it('Should get data and pass through subject', () => {
    spyOn(loaderService, 'hideLoader').and.callThrough();
    dataService.getData();
    httpRequest.expectOne(environment.apiUrl).flush(mockData);

    expect(gettedData).toEqual(mockData);
    expect(loaderService.hideLoader).toHaveBeenCalled();
  });

  it('Should call get data after calling add contact', () => {
    const newContact: Contact = {
      _id: '3',
      favourite: false,
      firstName: 'Maria',
      secondNmae: 'Nikitina',
      thirdName: 'Viktorovna',
      phone: '75555555555',
    }

    spyOn(dataService, 'getData').and.callThrough();

    dataService.addContact(newContact);

    let request: TestRequest = httpRequest.match(environment.apiUrl)[0];
    request.flush({});

    expect(request.request.method).toEqual('POST');
    expect(dataService.getData).toHaveBeenCalled();
  });

  it('Should call get data after calling delete contact', () => {
    const deleteId: string = '1';

    spyOn(dataService, 'getData').and.callThrough();

    dataService.deleteContact(deleteId);
    let request: TestRequest = httpRequest.match(`${environment.apiUrl}/${deleteId}`)[0];
    request.flush({});

    expect(request.request.method).toEqual('DELETE');
    expect(dataService.getData).toHaveBeenCalled();
  });

  it('Should call get data after set favourite contact', () => {
    const favouriteContact: Contact = {
      '_id': '2',
      'favourite': true,
      'firstName': 'Petr',
      'secondNmae': 'Petrov',
      'thirdName': 'Petrovich',
      'phone': '71111111111',
    };

    spyOn(dataService, 'getData').and.callThrough();

    dataService.setFavourite(favouriteContact);
    let request: TestRequest = httpRequest.match(`${environment.apiUrl}/${favouriteContact._id}`)[0];
    request.flush({});

    expect(request.request.method).toEqual('PUT');
    expect(dataService.getData).toHaveBeenCalled();
  });
})