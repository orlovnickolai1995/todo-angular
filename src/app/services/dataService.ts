import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';
import { Contact } from '../models/contact.model';
import LoaderService from './loaderService';
import { environment } from '../../environments/environments';

@Injectable({
  providedIn: 'root',
})

/**
 * Класс для управления данными в приложении: получение, удаление, добавление контактов, так же установление избранных контактов 
 */
export class DataService {
  constructor(private http: HttpClient, private loaderService: LoaderService) {};

  public getContacts: Subject<Contact[]> = new Subject();

  /**
   * @description метод для получения записсей адресной книги из базы данных
   */
  public getData(): void {
    this.http.get<Contact[]>(environment.apiUrl).subscribe((value: Contact[]) => {
      this.getContacts.next(value);
      this.loaderService.hideLoader();
    });
  };

  /**
   * @description метод для добавления нового контакта в адерсную книгу
   * @param {Contact} contact контакт, подлежащий добавлению в базу данных
   */
  public addContact(contact: Contact): void {
    this.http.post(environment.apiUrl, contact).subscribe(() => this.getData());
  };

  /**
   * @description метод для удаления контакта из адресной книги
   * @param {string} id идентификатор удаляемого контакта 
   */
  public deleteContact(id: string): void {
    this.http.delete(`${environment.apiUrl}/${id}`).subscribe(() => this.getData());
  };

  /**
   * @description метод для установления контакта избранным
   * @param {Contact} contact контакты, который необходимо установить в качестве избранного
   */
  public setFavourite(contact: Contact): void {
    this.http.put(`${environment.apiUrl}/${contact._id}`, {...contact, favourite: !contact.favourite}).subscribe(() => this.getData());
  }
}