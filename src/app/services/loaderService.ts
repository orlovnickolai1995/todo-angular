import { Injectable } from "@angular/core";
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})

/**
 * Класс для управления отображением лоадера 
 */
export default class LoaderService {
  public isActive: Subject<boolean> = new Subject();

  /**
   * @description метод для показывания лоадера
   */
  public showLoader(): void {
    this.isActive.next(true);
  };

  /**
   * @description метод для скрытия лоадера
   */
  public hideLoader(): void {
    this.isActive.next(false);
  };
}