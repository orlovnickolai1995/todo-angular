import { Pipe, PipeTransform } from "@angular/core";
import Constants from '../services/constants';

@Pipe({
  name: 'phone'
})

/**
 * Класс для преобразования телефонов в нужный формат
 */
export class Phone implements PipeTransform {
  constructor(private constants: Constants) {}
  
  transform(phone: string): string {
    return phone ? phone.replace(this.constants.PHONE_FORMAT, "+7 ($1) $2-$3-$4") : '';
  }
}
