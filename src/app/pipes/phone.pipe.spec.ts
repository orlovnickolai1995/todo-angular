import { Phone } from './phone.pipe';
import Constants from '../services/constants';

describe('Transform phone to the valid format', () => {
  let pipe: Phone;

  beforeEach(() => {
    pipe = new Phone(new Constants());
  });

  it('Should return formated phone number', () => {
    expect(pipe.transform('9999999999')).toBe('+7 (999) 999-99-99');
  });

  it('Should return empty string', () => {
    expect(pipe.transform(null)).toBe('');
    expect(pipe.transform(undefined)).toBe('');
    expect(pipe.transform('')).toBe('');
  })
})