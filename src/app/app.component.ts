import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import LoaderService from './services/loaderService';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})

export class AppComponent implements OnInit, OnDestroy {
  public loading: boolean = true;

  private loaderSubscription: Subscription;

  constructor(private loaderService: LoaderService, private cdr: ChangeDetectorRef) {};

  ngOnInit(): void {
    this.loaderSubscription = this.loaderService.isActive.subscribe((value: boolean) => {      
      this.loading = value;
      this.cdr.markForCheck();
    });
  };

  ngOnDestroy(): void {
    this.loaderSubscription.unsubscribe();
  };
}
