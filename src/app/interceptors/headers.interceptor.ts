import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from "@angular/core";
import { Observable } from 'rxjs';
import { environment } from '../../environments/environments';

@Injectable({
  providedIn: 'root',
})

export class HeaderInterceptor implements HttpInterceptor {
  intercept(request: HttpRequest<Object>, next: HttpHandler): Observable<HttpEvent<Object>> {
    request = request.clone({
      setHeaders: {
        'x-apikey': environment.apiKey,
      }
    });

    return next.handle(request);
  }
}