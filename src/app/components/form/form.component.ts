import { ChangeDetectionStrategy, Component, OnInit } from "@angular/core";
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { DataService } from 'src/app/services/dataService';
import LoaderService from 'src/app/services/loaderService';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})

export class FormComponent implements OnInit {
  constructor(private dataService: DataService, private loaderService: LoaderService) {}
  
  public form: FormGroup;

  ngOnInit(): void {    
    this.form = new FormGroup({
      secondName: new FormControl(null, [
        Validators.required,
      ]),
      firstName: new FormControl(null),
      thirdName: new FormControl(null),
      phone: new FormControl(null, [
        Validators.required,
        Validators.pattern(/\d{10}/),
      ])
    });
  };
 
  /**
   * @description метод для добавления нового контакта в адресную книгу
   */
  addContact(): void {    
    if (this.form.valid) {
      this.loaderService.showLoader();
      this.dataService.addContact(this.form.value);
      this.form.reset();
    } else {
      this.form.markAllAsTouched();
    }
  };
}