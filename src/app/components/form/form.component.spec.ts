import { HttpClientModule } from '@angular/common/http';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import LoaderService from 'src/app/services/loaderService';
import { DataService } from '../../services/dataService';
import { FormComponent } from './form.component';

describe('Form component', () => {
  let fixture: ComponentFixture<FormComponent>;
  let dataService: DataService;
  let loaderService: LoaderService;
  let component: FormComponent;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        FormComponent,
      ],
      imports: [
        ReactiveFormsModule,
        FormsModule,
        HttpClientModule,
      ]
    })
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    dataService = TestBed.inject<DataService>(DataService);
    loaderService = TestBed.inject<LoaderService>(LoaderService);
  })

  afterEach(() => {
    fixture = null;
    dataService = null;
    loaderService = null;
  });

  it ('After creating component form should be not valid', () => {
    expect(component.form.valid).toBeFalse();
  });

  it('After insert not valid data form shoud be not valid', () => {
    component.form.controls['secondName'].setValue('sadsadsadsa');
    component.form.controls['phone'].setValue('1213');
    expect(component.form.valid).toBeFalse();

    component.form.controls['secondName'].setValue('sadsadsadsa');
    component.form.controls['phone'].setValue('79999999999');
    expect(component.form.valid).toBeTrue();
  });

  it('Shouldnt create new phone', () => {
    spyOn(component.form, 'markAllAsTouched').and.callThrough();
    spyOn(component.form, 'reset').and.callThrough();
    spyOn(dataService, 'addContact').and.callThrough();
    spyOn(loaderService, 'showLoader').and.callThrough();

    component.form.controls['secondName'].setValue('Jasmine');
    component.form.controls['firstName'].setValue('Jasmine');
    component.form.controls['thirdName'].setValue('Jasmine');
    component.form.controls['phone'].setValue('sadsadsadsa');

    component.addContact();

    expect(component.form.markAllAsTouched).toHaveBeenCalled();

    component.form.controls['secondName'].setValue('Ivanov');
    component.form.controls['firstName'].setValue('Ivan');
    component.form.controls['thirdName'].setValue('Ivanovich');
    component.form.controls['phone'].setValue('79999999999');

    component.addContact();

    expect(component.form.reset).toHaveBeenCalled();
    expect(dataService.addContact).toHaveBeenCalled();
    expect(loaderService.showLoader).toHaveBeenCalled();
    Object.keys(component.form.value).forEach(field => {
      expect(component.form.value[field]).toBeNull();
    });
  })
})