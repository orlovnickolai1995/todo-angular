import { HttpClientModule } from '@angular/common/http';
import { DebugElement } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { Contact } from 'src/app/models/contact.model';
import { Phone } from 'src/app/pipes/phone.pipe';
import { DataService } from 'src/app/services/dataService';
import LoaderService from 'src/app/services/loaderService';
import { DataTableComponent } from './list.component';

describe('Data table component', () => {
  let fixture: ComponentFixture<DataTableComponent>;
  let dataService: DataService;
  let loaderService: LoaderService;
  let component: DataTableComponent;
  const mockData: Contact[] = [
    {
      '_id': '1',
      'favourite': false,
      'firstName': 'Ivan',
      'secondNmae': 'Ivanov',
      'thirdName': 'Ivanovich',
      'phone': '79999999999',
    },
    {
      '_id': '2',
      'favourite': true,
      'firstName': 'Petr',
      'secondNmae': 'Petrov',
      'thirdName': 'Petrovich',
      'phone': '71111111111',
    }
  ]

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        DataTableComponent,
        Phone,
      ],
      imports: [
        HttpClientModule,
      ],
    });
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DataTableComponent);
    component = fixture.componentInstance;
    dataService = TestBed.inject<DataService>(DataService);
    loaderService = TestBed.inject<LoaderService>(LoaderService);
    fixture.detectChanges();
  });

  afterEach(() => {
    fixture = null;
    dataService = null;
    loaderService = null;
  });

  it('Should get and sort data', () => {
    expect(component.getDataSubscription).toBeTruthy();
    dataService.getContacts.next(mockData);
    expect(component.contacts).toEqual(mockData);
    expect(component.contacts.length).toEqual(2);
    fixture.detectChanges();

    const contacts: DebugElement[] = fixture.debugElement.queryAll(By.css('.item'));
    expect(contacts[0].nativeElement.innerText.indexOf('Petr')).toBeTruthy();
  });

  it('Should call delete', () => {
    spyOn(loaderService, 'showLoader').and.callThrough();
    spyOn(component, 'deleteContact').and.callThrough();
    dataService.getContacts.next(mockData);
    fixture.detectChanges();

    const deleteButtons: DebugElement[] = fixture.debugElement.queryAll(By.css('.item__delete-btn'));
    
    expect(deleteButtons.length).toEqual(2);

    deleteButtons[0].nativeElement.click();
    fixture.detectChanges();

    expect(component.deleteContact).toHaveBeenCalled();
  });

  it('Should set favourite', () => {
    spyOn(component, 'setFavourite').and.callThrough();
    dataService.getContacts.next(mockData);
    fixture.detectChanges();

    const favouriteButtons: DebugElement[] = fixture.debugElement.queryAll(By.css('.item__favourite'));
    
    expect(favouriteButtons.length).toEqual(1);

    favouriteButtons[0].nativeElement.click();
    fixture.detectChanges();

    expect(component.setFavourite).toHaveBeenCalled();
  });
})