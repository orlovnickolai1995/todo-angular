import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit } from "@angular/core";
import { Subscription } from 'rxjs';
import { Contact } from 'src/app/models/contact.model';
import { DataService } from 'src/app/services/dataService';
import LoaderService from 'src/app/services/loaderService';

@Component({
  selector: 'app-data-table',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss',],
  changeDetection: ChangeDetectionStrategy.OnPush,
})

export class DataTableComponent implements OnInit, OnDestroy {
  constructor(private dataService: DataService, private cdr: ChangeDetectorRef, private loaderService: LoaderService) {}

  public getDataSubscription: Subscription;

  public contacts: Contact[] = [];

  ngOnInit(): void {
    this.getDataSubscription = this.dataService.getContacts.subscribe((value: Contact[]) => {
      this.contacts = value.sort(contact => contact.favourite ? -1 : 1);
      this.cdr.markForCheck();
    });

    this.loaderService.showLoader();

    this.dataService.getData();
  };

  /**
   * @descriptions метод для удаления контакта из адресной книги
   * @param {string} id идентификатор удаляемого контакта
   */
  public deleteContact(id: string) {
    this.loaderService.showLoader();
    this.dataService.deleteContact(id);
  };

  /**
   * @description метод для установления избранного контакта
   * @param {Contact} contact параметр контакта, который необходимо сделать избранным 
   */
  public setFavourite(contact: Contact) {
    this.loaderService.showLoader();
    this.dataService.setFavourite(contact);
  };

  ngOnDestroy(): void {
    this.getDataSubscription.unsubscribe();
  };

  trackByFn(item: Contact, index: number): string {
    return item._id;
  };
};